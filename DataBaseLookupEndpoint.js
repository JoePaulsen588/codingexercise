var http = require("http");
var url = require("url");
var mysql = require("mysql");

//the only testing function with my test database that provides a meaningful result probably is
//http://127.0.0.1/database?location=WASHINGTON_DC

function lookup(queryData,connection,callback) {
	if (queryData.location)
	{
		var location = queryData.location;
		connection.query('SELECT name,profession from users where location = \'' + queryData.location + '\' order by profession', function(err, rows, fields) {
			if (err) throw err;
			
			callback(rows);
		});
	}
	else
	{
		callback(-1);
	}
}

exports.lookup = lookup;