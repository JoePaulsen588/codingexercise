ToC:

1. Notes
2. instructions for use
3. Code explanation by file
4. Explanations for e-mail questions

1. Okay, here is the source code for coding exercise!  As you said it should only take 75 minutes or so, I only used about an
hour and a half to do the actual module programming and testing (although I spent much longer in total learning node.js and asynchronous functions).
Thus, the code doesn't really have exhaustive error checking and whatnot.  Additionally, while I'm sure there is appropriate
software for unit testing, appropriate usage of conf files for setting up default variables, and various techniques for git version control I could have used, 
this is just a first interview stage, and I didn't want to go overboard.

2.For the test database, just setup a MySQL database and load up users.sql.  For the code, you will need to 'npm install node-mysql', and
I believe that is all.  The database specifics will need to be changed for you in codingExercise.js (host/password/etc)
Then just 'node codingExercise' and you're set to go.  It was also compiled and run on windows, so running on linux might result in some problems.

3.

codingExercise.js - This just sets up the database connection for testing, as well as starting an http server in Server.js

Server.js - This sets up the main testing environment.  I just used http navigation to various pages for testing.  For everything
but the database look up, just navigate to the appropriate path showed in the source (example: 127.0.0.1/dependencies).  
For the database lookup you will need to add an http query with variables (example provided in the DBLookup .js)

AuthenticationCheck.js - This just takes the database connection in codingExercise.js, runs the test json in server, and spits out
a bool result.  The server then prints to the page and console a message.  If the exercise was in a later stage in the interview, I guess I
could've done a more "real" authentication (appache html?), but this seemed appropriate for a first stage.

DataBaseLookupEndpoint.js - Similar to previous, nothing complicated.  For grouping by profession, I just used "order by" as it was a quick way to
get a form of grouping.  The code has an example http navigation to test 

EndpointCheck.js - As I real only used the database, I just checked the database connection for a dropped connection.  This was proving very
annoying to test, as I would have to start the database, then manually cut the connection, then test it out, and it didn't seem to work with that test 
(shutting down the databse would instantly cause the program to throw an error). As this was truly cumbersome to get a correct answer (it may even be correct
as it is similar to other solutions I found on google), I felt leaving it in this state with an explanation was adequate.  If the program were
dependent on more outside sources, I would return either -1 for "everything fine" OR an array filled with the names (string or enum val)
of those sources not working.

ReturnDirectories- Easy peasy.  Admittedly, it does the file check using the server as the root, but I didn't want to get too complicated.

4.

Tech choice:

Node.js was chosen because, while I have used java and python a little, it seemed more geared toward the json/database questions
which involved all but the final endpoint (which only took a few minutes).  
I wasn't expecting to spend all of Wednesday learning about http servers, asynchronous functions and callbacks, though :/  Apparently it's used
a lot, so at least I know another useful language now I suppose.

MySQL was chosen for the database because, while the node.js tutorials generally use Mongo, I knew MySql enough to not have to really think about the queries
or setting up the test db, I could just do it off hand.  In a larger project, I guess I would've tried using Mongo.  All the documentation
and tutorials for node.js seem to reference it, so it must be for a good reason.

Versioning:

Use git.  In all seriousness, I do not have the experience with git to appropriately answer this without spending a few hours learning
more about it.

Pagination:

A brief google search says that with Node.js, I should use AJAX.  So I would use this.
If you meant just to the console, I would do something like this using Limit:

http://www.hacksparrow.com/mysql-pagination.html

Compiles:

Should.  If it doesn't, let me know.

Unit Tests:

As I said earlier, you will have to manually change the few lines in code, or in the case of the user,location,profession query, navigate 
using an appropriate url query.