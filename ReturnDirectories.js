var fs = require('fs');

function theFiles(path,callback){
	fs.readdir( path, function (err, files) { 
		if (!err) 
			callback(files);
		else 
			throw err; 
	});
}
	
exports.theFiles = theFiles;