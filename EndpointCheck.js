var mysql = require("mysql");

function checkConnection(connection,callback){
	connection.query('SELECT 1', function(err, rows, fields) {
		if (err)
		{
			if (err.code == 'PROTOCOL_CONNECTION_LOST')
			{
				callback(false);
			}
			else
			{
				throw err;
			}
		}
		callback(true);
	});
}
	
exports.checkConnection = checkConnection;