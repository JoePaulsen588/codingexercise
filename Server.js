var http = require("http");
var url = require("url");
var mysql = require("mysql");


var databaseLookup = require("./DataBaseLookupEndpoint");
var returnDirectories = require("./ReturnDirectories");
var check = require("./EndpointCheck");
var auth = require("./AuthenticationCheck");

function start(connection) {
  function onRequest(request, response) {
    console.log("Request received.");
	var pathname = url.parse(request.url).pathname;
	var queryData = url.parse(request.url, true).query;
	
	/*  connection.query('SELECT name,profession from users where location = \'Washington D.C.\' ', function(err, rows, fields) {
			if (err) throw err;
			
			rows.forEach(function(element) {
				console.log(element);
			});
		});*/
	
	if (pathname === '/database')
	{
			databaseLookup.lookup(queryData,connection,function(result)
			{
			console.log('done');
			if (result != -1)
				{
					response.writeHead(200, {"Content-Type": "text/plain"});
					result.forEach(function(element) {
						console.log(element);
						response.write(element.name + ' ' + element.profession + '\n');
					});
					response.end();
					
				}
				else
				{
					console.log('error');
				}		
			});
		/*var test = "default";
		if (queryData.testVar)
		{
			test = queryData.testVar;
		}
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write("Hello World" + test);
		response.end();*/
	}
	else if (pathname === '/listing')
	{
		var path = "Desktop/castle";
		returnDirectories.theFiles(path,function(files){
			response.writeHead(200, {"Content-Type": "text/plain"});
			files.forEach(function(element) {
				console.log(element);
				response.write(element);
			});
			response.end();
		});
	}
	else if (pathname === '/dependencies')
	{
		check.checkConnection(connection,function(status)
		{
			response.writeHead(200, {"Content-Type": "text/plain"});
			var message;
			if (status)
			{
				message = "All systems go";
			}
			else
			{
				message = "The system is down";
			}
			console.log(message);
			response.write(message);
			response.end();
		});
	}
	else if (pathname === '/authentication')
	{
		var toJSON = '{"name": "OBAMA","password": "AFFORDABLE_CARE_ACT"}';
		auth.authenticationCheck(toJSON,connection,function(exists){
			response.writeHead(200, {"Content-Type": "text/plain"});
			var message;
			if (exists)
			{
				message = 'User Authenticated!';
			}
			else
			{
				message = 'Incorrect Login/password';
			}
			console.log(message);
			response.write(message);
			response.end();
		});
	}
	else
	{
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write("Welcome to the coding exercise \n");
		response.end();
	}
  }

  http.createServer(onRequest).listen(80);
  console.log("Server has started.");
}

exports.start = start;